import ZooBreadcrumb from "../../../components/breadcrumb";
import CreatePetForm from "../../../components/createPetForm";

const links = [
  { url: "/", name: "Home" },
  { url: "/animals", name: "Animals" },
  { url: "/animals/create", name: "Create" },
];

function CreatePet() {
  return (
    <div className="h-100 w-100 p-5">
      <ZooBreadcrumb links={links} />
      <CreatePetForm></CreatePetForm>
    </div>
  );
}

export default CreatePet;
