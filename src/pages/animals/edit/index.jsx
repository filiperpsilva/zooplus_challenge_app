import ZooBreadcrumb from "../../../components/breadcrumb";
import EditPetForm from "../../../components/editPetForm";

const links = [
  { url: "/", name: "Home" },
  { url: "/animals", name: "Animals" },
  { url: "/animals/edit", name: "Edit" },
];

function EditPetPage() {
  return (
    <div className="h-100 w-100 p-5">
      <ZooBreadcrumb links={links} />
      <EditPetForm></EditPetForm>
    </div>
  );
}

export default EditPetPage;
