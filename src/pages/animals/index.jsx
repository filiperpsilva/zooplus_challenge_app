import {
  Button,
  Flex,
  Link,
  Spacer,
  Spinner,
  Wrap,
  WrapItem,
} from "@chakra-ui/react";
import ZooBreadcrumb from "../../components/breadcrumb/";
import PetCard from "../../components/card";
import { useGetAnimals } from "../../hooks/useGetAnimals";
import { useEffect } from "react";

function Animals() {
  const links = [
    { url: "/", name: "Home" },
    { url: "/animals", name: "Animals" },
  ];

  const { getAnimals, animals, isLoading } = useGetAnimals();
  useEffect(() => {
    getAnimals();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <div className="h-100 w-100 p-5">
      <Flex>
        <ZooBreadcrumb links={links} />
        <Spacer />
        <Link href="/animals/create">
          <Button
            mr={0}
            _hover={{
              bg: "#555",
              color: "white",
            }}
            colorScheme="blackAlpha"
          >
            + Add Pet
          </Button>
        </Link>
      </Flex>
      <hr />
      {isLoading && (
        <div className="w-100 mt-3 d-flex justify-content-center ">
          <Spinner
            data-testid="pets-spinner"
            thickness="4px"
            emptyColor="#CCC"
            size="lg"
          />
        </div>
      )}

      <Wrap>
        {animals &&
          animals.length > 0 &&
          animals.map((animal) => (
            <WrapItem key={animal.id}>
              <PetCard petData={animal} />
            </WrapItem>
          ))}
      </Wrap>

      {!isLoading && animals.length === 0 && (
        <div className="w-100 mt-3 d-flex justify-content-center ">
          NO PETS FOUND!
        </div>
      )}
    </div>
  );
}

export default Animals;
