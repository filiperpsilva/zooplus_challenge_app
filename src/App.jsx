import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import "./App.css";
import { ChakraProvider } from "@chakra-ui/react";
import CreatePet from "./pages/animals/create";
import Animals from "./pages/animals";
import Navbar from "./components/navbar";
import EditPetPage from "./pages/animals/edit";

function App() {
  return (
    <ChakraProvider>
      <BrowserRouter>
        <div style={{ height: "100vh" }} className="d-flex flex-row">
          <Navbar></Navbar>
          <Routes>
            <Route path="animals" element={<Animals />}></Route>
            <Route path="animals/create" element={<CreatePet />}></Route>
            <Route path="animals/edit/*" element={<EditPetPage />}></Route>
            <Route path="/" element={<Navigate to="animals" replace />}></Route>
          </Routes>
        </div>
      </BrowserRouter>
    </ChakraProvider>
  );
}

export default App;
