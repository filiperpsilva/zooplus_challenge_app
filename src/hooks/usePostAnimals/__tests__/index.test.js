import { render, screen, waitFor } from "@testing-library/react";
import { usePostAnimals } from "..";
import * as axios from "axios";
import { useEffect } from "react";
import * as useShowToastModule from "../../useToast";

jest.mock("axios");

const animalData = {
  name: "Bunny",
  breed: "rabbit",
  type: "bunny",
  gender: "female",
};
function Dummy({ animalData, showApiResponseMessage }) {
  const { isLoading, postAnimal, error, status } = usePostAnimals();

  useEffect(() => {
    postAnimal(animalData, { showApiResponseMessage });
  }, []);

  return (
    <>
      <div>{isLoading ? "isLoading" : "isNotLoading"}</div>
      <div>{error ? error : "no error"}</div>
      <div>{status}</div>
    </>
  );
}

describe("usePostAnimals", () => {
  it("Should return 201 status if the request is sucess", async () => {
    jest.spyOn(useShowToastModule, "useShowToast").mockReturnValue({
      showToast: jest.fn(),
    });

    axios.post.mockImplementation(
      () =>
        new Promise((resolve) => {
          resolve({ status: 201 });
        })
    );
    render(<Dummy animalData={animalData} />);

    await waitFor(() => {
      expect(screen.getByText("201")).toBeTruthy();
      expect(screen.getByText("isNotLoading")).toBeTruthy();
      expect(axios.post).toBeCalledTimes(1);
      expect(screen.getByText("no error")).toBeTruthy();
    });
  });

  it("Should return status 500 if any error happened", async () => {
    jest.spyOn(useShowToastModule, "useShowToast").mockReturnValue({
      showToast: jest.fn(),
    });
    axios.post.mockImplementation(
      () =>
        new Promise((_, reject) => {
          reject({ status: 500, message: "Api call not completed" });
        })
    );
    render(<Dummy animalData={animalData} />);

    await waitFor(() => {
      expect(screen.getByText("500")).toBeTruthy();
      expect(screen.getByText("isNotLoading")).toBeTruthy();
      expect(axios.post).toBeCalledTimes(1);
      expect(screen.getByText("Api call not completed")).toBeTruthy();
    });
  });

  it("Should call the postAnimal function and do not show toast", async () => {
    jest.spyOn(useShowToastModule, "useShowToast").mockReturnValue({
      showToast: jest.fn(),
    });
    axios.post.mockImplementation(
      () =>
        new Promise((_, reject) => {
          reject({ status: 500, message: "Api call not completed" });
        })
    );
    render(<Dummy showApiResponseMessage={false} animalData={animalData} />);

    await waitFor(() => {
      expect(axios.post).toBeCalledTimes(1);
      expect(useShowToastModule.useShowToast().showToast).toBeCalledTimes(0);
    });
  });
});
