import axios from "axios";
import { useState } from "react";
import { useShowToast } from "../useToast";

export function usePostAnimals() {
  const [status, setStatus] = useState();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState();
  const { showToast } = useShowToast();

  const postAnimal = (data, { showApiResponseMessage = true } = {}) => {
    setIsLoading(true);
    axios
      .post("http://localhost:3001/animals", {
        ...data,
      })
      .then((response) => {
        setError(null);
        setStatus(response.status);

        if (showApiResponseMessage) {
          showToast({
            title: "Animal created",
            description: "You have created an Animal",
            status: "success",
          });
        }
      })
      .catch((response) => {
        setError(response.message);
        setStatus(response.status);

        if (showApiResponseMessage) {
          showToast({
            title: "Error",
            description: "Something went wrong, animal not created",
            status: "error",
          });
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  return { status, isLoading, postAnimal, error };
}
