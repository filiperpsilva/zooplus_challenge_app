import axios from "axios";
import { useState } from "react";
import { useShowToast } from "../useToast";

export function usePutAnimal() {
  const [status, setStatus] = useState();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState();
  const { showToast } = useShowToast();

  const putAnimal = (data, { showApiResponseMessage = true } = {}) => {
    console.log({ data });
    setIsLoading(true);
    axios
      .put("http://localhost:3001/animals/" + data.id, {
        ...data,
      })
      .then((response) => {
        setError(null);
        setStatus(response.status);

        if (showApiResponseMessage) {
          showToast({
            title: "Animal updated",
            description: "You have updated an Animal",
            status: "success",
          });
        }
      })
      .catch((response) => {
        setError(response.message);
        setStatus(response.status);

        if (showApiResponseMessage) {
          showToast({
            title: "Error",
            description: "Something went wrong, animal not updated",
            status: "error",
          });
        }
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  return { status, isLoading, putAnimal, error };
}
