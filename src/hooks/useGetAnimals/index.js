import axios from "axios";
import { useState } from "react";

export function useGetAnimals() {
  const [status, setStatus] = useState();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState();

  const [animals, setAnimals] = useState([]);

  const getAnimals = () => {
    setIsLoading(true);
    axios
      .get("http://localhost:3001/animals")
      .then((response) => {
        setError(null);
        setAnimals(response.data.animals);
        setStatus(response.status);
      })
      .catch((response) => {
        setError(response.message);
        setAnimals([]);
        setStatus(response.status);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { status, isLoading, getAnimals, error, animals };
}
