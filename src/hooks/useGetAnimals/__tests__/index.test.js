import React, { useEffect } from "react";
import * as axios from "axios";
import { useGetAnimals } from "../index";
import { render, screen, waitFor } from "@testing-library/react";

jest.mock("axios");
const animals = [
  {
    id: "8f71918a-f070-4bef-9c80-56b5876e4268",
    name: "Ted",
    type: "dog",
    breed: "labrador",
    gender: "male",
    vaccinated: false,
    lastVisit: null,
    lastUpdate: null,
    createdAt: "2023-03-27T10:08:49.604Z",
  },
  {
    id: "64be80f1-a4ad-4fdf-b0e9-30af1188569b",
    name: "Misty",
    type: "cat",
    breed: "sphinx",
    gender: "female",
    vaccinated: false,
    lastVisit: null,
    lastUpdate: null,
    createdAt: "2023-03-27T10:10:10.058Z",
  },
];

function Dummy({ isToCallGetAnimals }) {
  const { isLoading, getAnimals, animals, error, status } = useGetAnimals();
  useEffect(() => {
    if (isToCallGetAnimals) {
      getAnimals();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <>
      <div>{isLoading ? "true" : "false"}</div>
      <div>{error ? error : "no error"}</div>
      <div>{status}</div>
      <div>{JSON.stringify(animals)}</div>
    </>
  );
}

describe("useGetAnimals", () => {
  it("Should return the animals list", async () => {
    axios.get.mockImplementation(
      () =>
        new Promise((resolve) => {
          resolve({ data: animals, status: 200 });
        })
    );
    render(<Dummy isToCallGetAnimals={true} />);

    await waitFor(() => {
      expect(axios.get).toBeCalledTimes(1);
      expect(screen.getByText("false")).toBeTruthy();
      expect(screen.getByText("200")).toBeTruthy();
      expect(screen.getByText("no error")).toBeTruthy();
    });
  });

  it("Should not display animal data after the API returned an error", async () => {
    axios.get.mockImplementation(
      () =>
        new Promise((_, reject) => {
          reject({ data: [], status: 500, message: "Error fetching the api" });
        })
    );
    render(<Dummy isToCallGetAnimals={true} />);

    await waitFor(() => {
      expect(screen.getByText(JSON.stringify([]))).toBeTruthy();
      expect(axios.get).toBeCalledTimes(1);
      expect(screen.getByText("false")).toBeTruthy();
      expect(screen.getByText("500")).toBeTruthy();
      expect(screen.getByText("Error fetching the api")).toBeTruthy();
    });
  });
});
