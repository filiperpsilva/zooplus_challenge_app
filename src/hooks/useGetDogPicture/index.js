import axios from "axios";
import { useState } from "react";

export function useGetDogPicture() {
  const [status, setStatus] = useState();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState();

  const [dogPicture, setdogPicture] = useState("");

  const getDogPicture = () => {
    setIsLoading(true);
    axios
      .get("https://dog.ceo/api/breeds/image/random")
      .then((response) => {
        setError(null);
        setdogPicture(response.data.message);
        setStatus(response.status);
      })
      .catch((response) => {
        setError(response.message);
        setdogPicture(
          "https://images.dog.ceo/breeds/labrador/n02099712_3197.jpg"
        );
        setStatus(response.status);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { status, isLoading, getDogPicture, error, dogPicture };
}
