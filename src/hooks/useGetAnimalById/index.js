import axios from "axios";
import { useState } from "react";

export function useGetAnimalById() {
  const [status, setStatus] = useState();
  const [error, setError] = useState();
  const [isLoading, setIsLoading] = useState();

  const [animal, setAnimal] = useState({});

  const getAnimalById = (id) => {
    setIsLoading(true);
    axios
      .get("http://localhost:3001/animals/" + id)
      .then((response) => {
        setError(null);
        setAnimal(response.data.animal);
        setStatus(response.status);
      })
      .catch((response) => {
        setError(response.message);
        setAnimal({});
        setStatus(response.status);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  return { status, isLoading, getAnimalById, error, animal };
}
