import React, { useEffect } from "react";
import * as axios from "axios";
import { useGetAnimalById } from "../index";
import { render, screen, waitFor } from "@testing-library/react";

jest.mock("axios");

const animal = {
  id: "8f71918a-f070-4bef-9c80-56b5876e4268",
  name: "Ted",
  type: "dog",
  breed: "labrador",
  gender: "male",
  vaccinated: false,
  lastVisit: null,
  lastUpdate: null,
  createdAt: "2023-03-27T10:08:49.604Z",
};

function Dummy({ animalId }) {
  const { isLoading, getAnimalById, animal, error, status } = useGetAnimalById();
  useEffect(() => {
    getAnimalById(animalId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [animalId]);

  return (
    <>
      <div>{isLoading ? "true" : "false"}</div>
      <div>{error ? error : "no error"}</div>
      <div>{status}</div>
      <div>{JSON.stringify(animal)}</div>
    </>
  );
}

describe("useGetAnimalById", () => {
  it("Should return the animal with the specified ID", async () => {
    axios.get.mockImplementationOnce(
      () =>
        new Promise((resolve) => {
          resolve({ data: animal, status: 200 });
        })
    );

    const animalId = animal.id;
    render(<Dummy animalId={animalId} />);

    await waitFor(() => {
      expect(axios.get).toBeCalledWith(`http://localhost:3001/animals/${animalId}`);
      expect(screen.getByText("false")).toBeTruthy();
      expect(screen.getByText("200")).toBeTruthy();
      expect(screen.getByText("no error")).toBeTruthy();
    });
  });

  it("Should display an error message when the API returns an error", async () => {
    const errorMessage = "Error fetching animal data";
    axios.get.mockImplementationOnce(
      () =>
        new Promise((_, reject) => {
          reject({ data: {}, status: 404, message: errorMessage });
        })
    );

    const animalId = animal.id;
    render(<Dummy animalId={animalId} />);

    await waitFor(() => {
      expect(screen.getByText(errorMessage)).toBeTruthy();
      expect(axios.get).toBeCalledWith(`http://localhost:3001/animals/${animalId}`);
      expect(screen.getByText("Error fetching animal data")).toBeTruthy();
      expect(screen.getByText("404")).toBeTruthy();
    });
  });
});
