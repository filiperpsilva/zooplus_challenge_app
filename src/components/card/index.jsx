import {
  Heading,
  Avatar,
  Box,
  Center,
  Text,
  Stack,
  Button,
  Link,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useGetDogPicture } from "../../hooks/useGetDogPicture";

function PetCard({ petData }) {
  const { dogPicture, getDogPicture } = useGetDogPicture();
  useEffect(() => {
    getDogPicture();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Center p={6}>
      <Box
        maxW={"450px"}
        w={"xs"}
        bg="white"
        boxShadow={"2xl"}
        rounded={"lg"}
        p={6}
        textAlign={"center"}
      >
        <Avatar size={"xl"} src={dogPicture} mb={4} pos={"relative"} />
        <Heading fontSize={"2xl"}>{petData.name}</Heading>
        <Text fontWeight="medium" color={"gray"} mb={4}>
          Last visit: {petData.lastVisit && petData.lastVisit.split("T")[0]}
        </Text>
        <Text textAlign="center" px={3}>
          <span>
            <b>Type: </b>
            {petData.type}
          </span>
          <br />
          <span>
            <b>Breed: </b>
            {petData.breed}
          </span>
          <br />
          <span>
            <b>Gender: </b>
            {petData.gender}
          </span>
          <br />
          <span>
            <b>Vaccinated?: </b>
            {petData.vaccinated === true ? "Yes" : "No"}
          </span>
        </Text>

        <Link href={"/animals/edit/" + petData.id}>
          <Stack mt={8} direction={"row"} spacing={4}>
            <Button
              flex={1}
              fontSize={"sm"}
              rounded={"full"}
              _focus={{
                bg: "gray",
              }}
            >
              Edit
            </Button>
          </Stack>
        </Link>
      </Box>
    </Center>
  );
}

export default PetCard;
