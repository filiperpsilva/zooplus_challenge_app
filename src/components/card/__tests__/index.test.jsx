import { render, screen } from "@testing-library/react";
import PetCard from "..";

describe("<PetCard/>", () => {
  const petData = {
    id: 1,
    name: "Fido",
    lastVisit: "2022-04-09T10:00:00.000Z",
    type: "Dog",
    breed: "Labrador",
    gender: "Male",
    vaccinated: true,
  };

  it("renders pet name and type", () => {
    render(<PetCard petData={petData} />);
    const petName = screen.getByText(petData.name);
    const petType = screen.getByText(petData.type);
    
    expect(petName).toBeInTheDocument();
    expect(petType).toBeInTheDocument();
  });

  it("renders pet data", () => {
    render(<PetCard petData={petData} />);
    const petBreed = screen.getByText(/breed/i);
    const petGender = screen.getByText(/gender/i);
    const petVaccinated = screen.getByText(/vaccinated/i);
   
    expect(petBreed).toBeInTheDocument();
    expect(petGender).toBeInTheDocument();
    expect(petVaccinated).toBeInTheDocument();
  });

  it("renders edit button", () => {
    render(<PetCard petData={petData} />);
    const editButton = screen.getByRole("button", { name: /edit/i });
    
    expect(editButton).toBeInTheDocument();
  }); 

  it('renders last visit date if present', () => {
    render(<PetCard petData={{ name: 'Buddy', lastVisit: '2022-02-14T10:20:30Z' }} />);
    const lastVisitText = screen.getByText(/last visit:/i);
    const dateText = lastVisitText.textContent;
    
    expect(lastVisitText).toBeInTheDocument();
    expect(dateText).toMatch(/\d{4}-\d{2}-\d{2}/);
  });

  it('does not renders last visit date if not present', () => {
    render(<PetCard petData={{ name: 'Buddy', lastVisit: null }} />);
    const lastVisitText = screen.getByText(/last visit:/i);
    const dateText = lastVisitText.textContent;
    
    expect(lastVisitText).toBeInTheDocument();
    expect(dateText).not.toMatch(/\d{4}-\d{2}-\d{2}/);
  });
});
