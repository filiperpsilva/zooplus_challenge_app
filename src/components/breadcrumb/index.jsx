import { BreadcrumbItem, Breadcrumb, BreadcrumbLink } from "@chakra-ui/react";

function ZooBreadcrumb({ links }) {
  return (
    <Breadcrumb>
      {links.map((link) => (
        <BreadcrumbItem key={link.name}>
          <BreadcrumbLink href={link.url}>{link.name}</BreadcrumbLink>
        </BreadcrumbItem>
      ))}
    </Breadcrumb>
  );
}

export default ZooBreadcrumb;
