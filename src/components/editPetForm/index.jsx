import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Select,
} from "@chakra-ui/react";
import { useState } from "react";
import { usePutAnimal } from "../../hooks/usePutAnimals";
import { useEffect } from "react";
import { useGetAnimalById } from "../../hooks/useGetAnimalById";

function EditPetForm() {
  const petId = window.location.pathname.split("/")[3];

  const { getAnimalById, animal } = useGetAnimalById();

  const [name, setName] = useState({
    value: animal.name,
    isInvalid: animal ? false : true,
    isTouched: animal ? true : false,
  });
  const [type, setType] = useState({
    value: animal.type,
    isInvalid: animal ? false : true,
    isTouched: animal ? true : false,
  });
  const [breed, setBreed] = useState({
    value: animal.breed,
    isInvalid: animal ? false : true,
    isTouched: animal ? true : false,
  });
  const [gender, setGender] = useState({
    value: animal.gender,
    isInvalid: animal ? false : true,
    isTouched: animal ? true : false,
  });
  
  const { isLoading, putAnimal } = usePutAnimal();

  const isToDisableButton = () => {
    const invalidFields = [name, type, breed, gender]
      .map(({ isInvalid }) => isInvalid)
      .filter((isInvalid) => isInvalid === true);

    const untouchedFields = [name, type, breed, gender]
      .map(({ isTouched }) => isTouched)
      .filter((isTouched) => isTouched === false);

    return untouchedFields.length > 0 || invalidFields.length > 0;
  };

  const handleSubmit = () => {
    putAnimal({
      id: animal.id,
      name: animal.name,
      type: animal.type,
      breed: animal.breed,
      gender: animal.gender,
      vaccinated: animal.vaccinated,
    });
  };

  useEffect(() => {
    getAnimalById(petId);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <FormControl isRequired className="p-5">
      <FormLabel>Name</FormLabel>
      <Input
        isInvalid={!name.isTouched ? false : name.isInvalid}
        onInput={(e) => {
          setName({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          });
          animal.name = e.target.value;
        }}
        value={animal.name ?? ""}
        placeholder="Enter name"
      />

      <FormLabel>Type</FormLabel>
      <Input
        isInvalid={!type.isTouched ? false : type.isInvalid}
        onInput={(e) => {
          setType({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          });
          animal.type = e.target.value;
        }}
        value={animal.type ?? ""}
        placeholder="Enter type"
      />

      <FormLabel>Breed</FormLabel>
      <Input
        isInvalid={!breed.isTouched ? false : breed.isInvalid}
        onInput={(e) => {
          setBreed({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          });
          animal.breed = e.target.value;
        }}
        value={animal.breed ?? ""}
        placeholder="Enter Breed"
      />

      <FormLabel>Gender</FormLabel>
      <Input
        isInvalid={!gender.isTouched ? false : gender.isInvalid}
        onInput={(e) => {
          setGender({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          });
          animal.gender = e.target.value;
        }}
        value={animal.gender ?? ""}
        placeholder="Enter Gender"
      />

      <FormLabel>Vaccinated?</FormLabel>
      <Select
        onChange={(e) => {
          animal.vaccinated = e.target.value;
        }}
        value={animal.vaccinated}
        placeholder="Is the pet vaccinated?"
      >
        <option default value="true">
          Yes
        </option>
        <option value="false"> No </option>
      </Select>

      <Button
        isDisabled={isToDisableButton()}
        type="submit"
        isLoading={isLoading}
        onClick={handleSubmit}
        className="mt-2 btn btn-black"
      >
        Submit
      </Button>
    </FormControl>
  );
}

export default EditPetForm;
