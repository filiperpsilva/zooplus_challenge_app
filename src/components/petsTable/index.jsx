import {
  Icon,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import { useEffect } from "react";
import { useGetAnimals } from "../../hooks/useGetAnimals";
import { MdEdit, MdDelete, MdSearch } from "react-icons/md";

function PetsTable() {
  const { isLoading, getAnimals, animals } = useGetAnimals();
  useEffect(() => {
    getAnimals();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <TableContainer>
        <Table
          className="table-bordered mt-4  "
          variant="striped"
          colorScheme="blackAlpha"
        >
          <Thead>
            <Tr>
              <Th scope="col">ID</Th>
              <Th scope="col">Name</Th>
              <Th scope="col">Gender</Th>
              <Th scope="col">Breed</Th>
              <Th scope="col">Type</Th>
              <Th scope="col">Vaccinated?</Th>
              <Th scope="col">Last Visit</Th>
              <Th scope="col">Options</Th>
            </Tr>
          </Thead>
          <Tbody>
            {animals &&
              animals.map((animal) => (
                <Tr key={animal.id}>
                  <Td scope="row">{animal.id}</Td>
                  <Td>{animal.name}</Td>
                  <Td>{animal.gender}</Td>
                  <Td>{animal.breed}</Td>
                  <Td>{animal.type}</Td>
                  <Td>{animal.vaccinated === true ? "Yes" : "No"}</Td>
                  <Td>{animal.lastVisit}</Td>
                  <Td>
                    <Icon mr="4" fontSize="16" as={MdEdit} />
                    <Icon mr="4" fontSize="16" as={MdDelete} />
                    <Icon mr="4" fontSize="16" as={MdSearch} />
                  </Td>
                </Tr>
              ))}
          </Tbody>
        </Table>
      </TableContainer>
      {isLoading === true && (
        <div className="w-100 mt-3 d-flex justify-content-center ">
          <Spinner
            data-testid="pets-spinner"
            thickness="4px"
            emptyColor="#CCC"
            size="lg"
          />
        </div>
      )}
      {!isLoading && (!animals || animals.length === 0) && (
        <div
          className="w-100 mt-3 d-flex justify-content-center"
          id="no-animals-found"
        >
          No animals found
        </div>
      )}
    </>
  );
}

export default PetsTable;
