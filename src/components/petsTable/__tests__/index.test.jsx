import { render, screen, waitFor } from "@testing-library/react";
import React from "react";
import PetsTable from "..";
import * as useGetAnimalsModule from "../../../hooks/useGetAnimals";

jest.setTimeout(40000);

const animals = [
  {
    id: "8f71918a-f070-4bef-9c80-56b5876e4268",
    name: "Ted",
    type: "dog",
    breed: "labrador",
    gender: "male",
    vaccinated: false,
    lastVisit: null,
    lastUpdate: null,
    createdAt: "2023-03-27T10:08:49.604Z",
  },
  {
    id: "64be80f1-a4ad-4fdf-b0e9-30af1188569b",
    name: "Misty",
    type: "cat",
    breed: "sphinx",
    gender: "female",
    vaccinated: false,
    lastVisit: null,
    lastUpdate: null,
    createdAt: "2023-03-27T10:10:10.058Z",
  },
];

describe("<PetsTable/>", () => {
  it("Should render the table with animal data", async () => {
    jest.spyOn(useGetAnimalsModule, "useGetAnimals").mockReturnValue({
      animals,
      getAnimals: () => [],
      isLoading: false,
    });

    render(<PetsTable />);

    const mockAnimals = useGetAnimalsModule.useGetAnimals().animals;
    const rows = screen.getAllByRole("row");

    // Doing rows.length - 1 to ignore  the header
    expect(rows.length - 1).toBe(mockAnimals.length);

    mockAnimals
      .flatMap(({ id, name, gender, breed }) => [id, name, gender, breed])
      .forEach((attribute) => {
        expect(screen.getAllByText(attribute)).toBeTruthy();
      });
  });

  it.each([null, undefined, []])(
    "Should render the message: 'No animals found' if the server response is %s",
    async (invalidResponse) => {
      jest.spyOn(useGetAnimalsModule, "useGetAnimals").mockReturnValue({
        animals: invalidResponse,
        getAnimals: () => [],
        isLoading: false,
      });

      render(<PetsTable />);

      expect(screen.getByText("No animals found")).toBeTruthy();
      expect(screen.getAllByText("No animals found").length).toBe(1);
    }
  );

  it("Should render the Spinner while fetching the animals", async () => {
    jest.spyOn(useGetAnimalsModule, "useGetAnimals").mockReturnValue({
      animals: [],
      getAnimals: () => [],
      isLoading: true,
    });

    render(<PetsTable />);

    expect(screen.getByTestId("pets-spinner")).toBeTruthy();
  });
});
