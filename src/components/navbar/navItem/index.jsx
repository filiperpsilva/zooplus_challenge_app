import { Flex, Icon, Link } from "@chakra-ui/react";

export default function NavItem({ icon, name, url }) {
  return (
    <Link href={url} style={{ textDecoration: "none" }}>
      <Flex
        align="center"
        p="4"
        mx="4"
        borderRadius="lg"
        cursor="pointer"
        _hover={{
          bg: "#555",
          color: "white",
        }}
      >
        {icon && <Icon mr="4" fontSize="16" as={icon} />}
        {name}
      </Flex>
    </Link>
  );
}
