import { Box, Flex, Text } from "@chakra-ui/react";
import NavItem from "../navItem";

export default function SidebarContent({ LinkItems }) {
  return (
    <Box
      className="bg-primary-color"
      borderRight="1px"
      borderRightColor="#CCC"
      pos="fixed"
      h="full"
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Text borderBottom="1px" fontSize="2xl" fontWeight="bold">
          ZOOHOTEL
        </Text>
      </Flex>
      {LinkItems.map((link) => (
        <NavItem
          name={link.name}
          url={link.url}
          key={link.name}
          icon={link.icon}
        ></NavItem>
      ))}
    </Box>
  );
}
