import { Box } from "@chakra-ui/react";

import { MdPets } from "react-icons/md";
import SidebarContent from "./sideBarContent";

export default function Navbar() {
  const linkItems = [{ name: "Animals", icon: MdPets, url: "/animals" }];

  return (
    <Box minH="100vh" w="10vw" bg="white">
      <SidebarContent LinkItems={linkItems} />
    </Box>
  );
}
