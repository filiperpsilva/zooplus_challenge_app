import {
  Button,
  FormControl,
  FormLabel,
  Input,
  Select,
} from "@chakra-ui/react";
import { useState } from "react";
import { usePostAnimals } from "../../hooks/usePostAnimals";

function CreatePetForm() {
  const [name, setName] = useState({
    value: "",
    isInvalid: true,
    isTouched: false,
  });
  const [type, setType] = useState({
    value: "",
    isInvalid: true,
    isTouched: false,
  });
  const [breed, setBreed] = useState({
    value: "",
    isInvalid: true,
    isTouched: false,
  });
  const [gender, setGender] = useState({
    value: "",
    isInvalid: true,
    isTouched: false,
  });
  const [vaccinated, setVaccinated] = useState({
    value: "false",
  });

  const { isLoading, postAnimal } = usePostAnimals();

  const isToDisableButton = () => {
    const invalidFields = [name, type, breed, gender]
      .map(({ isInvalid }) => isInvalid)
      .filter((isInvalid) => isInvalid === true);

    const untouchedFields = [name, type, breed, gender]
      .map(({ isTouched }) => isTouched)
      .filter((isTouched) => isTouched === false);

    return untouchedFields.length > 0 || invalidFields.length > 0;
  };

  const handleSubmit = () => {
    postAnimal(
      {
        name: name.value,
        type: type.value,
        breed: breed.value,
        gender: gender.value,
        vaccinated: vaccinated.value ? true:false,
      },
      { showApiResponseMessage: true }
    );
  };

  return (
    <FormControl isRequired className="p-5">
      <FormLabel>Name</FormLabel>
      <Input
        isInvalid={!name.isTouched ? false : name.isInvalid}
        onInput={(e) =>
          setName({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          })
        }
        value={name.value}
        placeholder="Enter name"
      />

      <FormLabel>Type</FormLabel>
      <Input
        isInvalid={!type.isTouched ? false : type.isInvalid}
        onInput={(e) =>
          setType({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          })
        }
        value={type.value}
        placeholder="Enter type"
      />

      <FormLabel>Breed</FormLabel>
      <Input
        isInvalid={!breed.isTouched ? false : breed.isInvalid}
        onInput={(e) =>
          setBreed({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          })
        }
        value={breed.value}
        placeholder="Enter Breed"
      />

      <FormLabel>Gender</FormLabel>
      <Input
        isInvalid={!gender.isTouched ? false : gender.isInvalid}
        onInput={(e) =>
          setGender({
            value: e.target.value,
            isInvalid: !Boolean(e.target.value),
            isTouched: true,
          })
        }
        value={gender.value}
        placeholder="Enter Gender"
      />

      <FormLabel>Vaccinated?</FormLabel>
      <Select
        onChange={(e) =>
          setVaccinated({
            value: e.target.value === "true" ? true : false,
          })
        }
        value={vaccinated.value}
        placeholder="Enter Gender"
      >
        <option default value="true">
          Yes
        </option>
        <option value="false"> No </option>
      </Select>

      <Button
        isDisabled={isToDisableButton()}
        type="submit"
        isLoading={isLoading}
        onClick={handleSubmit}
        className="mt-2 btn btn-black"
      >
        Submit
      </Button>
    </FormControl>
  );
}

export default CreatePetForm;
