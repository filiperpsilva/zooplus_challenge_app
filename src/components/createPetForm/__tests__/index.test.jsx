import { act, render, screen, waitFor } from "@testing-library/react";
import React from "react";
import * as usePostAnimalsModule from "../../../hooks/usePostAnimals";
import CreatePetForm from "..";
import userEvent from "@testing-library/user-event";

jest.setTimeout(40000);

const mandatoryInputPlaceholders = [
  "Enter name",
  "Enter type",
  "Enter Breed",
  "Enter Gender",
];

describe("<CreatePetForm />", () => {
  it("The 'submit' button should be disabled in the first render", async () => {
    render(<CreatePetForm />);
    const button = screen.getByRole("button");
    expect(button).toBeDisabled();
  });

  it("The 'submit' button should be enabled if all the fields have values", async () => {
    jest.spyOn(usePostAnimalsModule, "usePostAnimals").mockReturnValue({
      status: 200,
      postAnimal: jest.fn(),
      isLoading: false,
    });
    render(<CreatePetForm />);
    const postAnimal = usePostAnimalsModule.usePostAnimals().postAnimal;

    const button = screen.getByRole("button");
    expect(button).toBeDisabled();

    for (let i = 0; i < mandatoryInputPlaceholders.length; i++) {
      const input = screen.getByPlaceholderText(mandatoryInputPlaceholders[i]);
      
      await act(async () => {
        await userEvent.type(input, "some text");
      });
      const isLast = i + 1 === mandatoryInputPlaceholders.length;
      if (!isLast) {
        expect(button).toBeDisabled();
      }
    }

    userEvent.click(button);

    await waitFor(() => {
      expect(button).toBeEnabled();
      expect(postAnimal).toBeCalledTimes(1);
    }, 10000);
  });
});
