# Introduction - App
The project is containerized with Docker and it will run in a Docker Container. To build and run it, just simply run the command bellow: 

`sh ./setup.sh`

If setup.sh file is not executable, run the command below:
`chmod +x setup.sh`

This will build and run the container and expose it on the port 3000, being available in the address: 
[localhost:3000](localhost:3000 "localhost:3000")

# Solution
I've created a simple app to **list**, **edit** and **add** new animals. 

In the .gitlab folder I've created CI/CD jobs to automatically install, build, test and update my docker image in the **Amazon ECR** registry. The docker job will only run if commits are made into the main branch.

# Technologies
To create this project I've chosen to use **ReactJs** with **Chakra-UI**, using **Jest** to test the application.

### Known Problems
The application is not responsive, therefore it will break the layout if not in desktop view.